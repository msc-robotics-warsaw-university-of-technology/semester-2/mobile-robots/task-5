function [forwBackVel, leftRightVel, rotVel, finish] = ...
    solution5(pts, contacts, position, orientation, varargin)
    % The control loop callback function - the solution 
    % for Task 5
    % get the parameters
    if length(varargin) > 2
         error(['Wrong number of additional arguments:' ...
             ' %d\n'], length(varargin));
    end
    if length(varargin{1})~=2
        error('Goal Position must have x and y coordiantes');
    end
    dest = varargin{1};
    left_right=-1;
    if length(varargin)==2
        left_right=varargin{2};
    end

    % declare the persistent variable that keeps 
    % the state of the Finite
    % State Machine (FSM
    persistent state start_goal d stop_wait;
    if isempty(state)
        % the initial state of the FSM is 'init'
        state = 'init';
    end

    % initialize the robot control variables 
    % (returned by this function)
    finish = false;
    forwBackVel = 0;
    leftRightVel = 0;
    rotVel = 0;

     % TODO: manage the states of FSM
    u_max = 10;
    gain_P = 100;
    u_rad_max = 4;
    u_tan_max = 8;
    u_rot_max = 4;

     % process the LIDAR sensor data
    % get the laser contact points in sensor's coordinates
    points = [ pts(1,contacts)' pts(2,contacts)' ];
    % calculate the distances
    distances = (pts(1,contacts)'.^2 + pts(2,contacts)'.^2).^0.5;
    % get the closest point
    [min_value, min_index] = min(distances(:));
    position_diff=dest-position(1:2);

    if strcmp(state, 'init')
        state = 'move_straight';
        fprintf('changing FSM state to %s\n', state);
        start_goal = position_diff;
    elseif strcmp(state, 'move_straight')
        distance=norm(position_diff);
        v=position_diff./distance;
        u_pos = gain_P * distance;
        if u_pos > u_max 
            u_pos = u_max;
        elseif u_pos < -u_max
            u_pos = -u_max;
        end
        v=v*u_pos;
        Vel_glob = [v(2); v(1);orientation(3)];
        vel = rotationalMatrix(orientation(3))*Vel_glob;
        etheta = angdiff(atan2(vel(1),vel(2)),-pi/2);
        if abs(etheta)>0.05
            rotVel = u_rot_max*etheta;
        else
            forwBackVel = vel(1);
            leftRightVel = vel(2);
            error_dist = min_value - 0.5;
            
            if abs(error_dist)<0.05
                state="move_along_wall";
                fprintf('changing FSM state to %s\n', state);
                d = norm(position_diff);
                stop_wait=0;
            end
            if abs(norm(position_diff))<0.01
                state="finish";
                fprintf('changing FSM state to %s\n', state);
                finish=1;
            end
        end
    elseif strcmp(state, 'move_along_wall')
        min_dist_point = (points(min_index,:))';
        % calculate the angle to the closest point
        closest_angle = atan2(points(min_index,2),points(min_index,1));
    
        error_dist = min_value - 0.5;
        error_theta = angdiff(closest_angle,-pi/2);
        
        rad_dir = min_dist_point/norm(min_dist_point);
        tan_dir = Rotation_matrix(left_right*pi/2)*rad_dir;

        u_r = gain_P * error_dist;
        if u_r > u_rad_max
            u_r = u_rad_max;
        elseif u_r < -u_rad_max
            u_r = -u_rad_max;
        end

        u_t = u_tan_max;
        u_rot = gain_P *error_theta;
        if u_rot > u_rot_max
            u_rot = u_rot_max;
        elseif u_rot < -u_rot_max
            u_rot = -u_rot_max;
        end

        Vel = rad_dir * u_r + tan_dir * u_t;
        
        forwBackVel = Vel(2);
        leftRightVel = Vel(1);
        rotVel = u_rot;
        stop_wait = stop_wait + 1;
        l=sum(start_goal.*( [-position_diff(2) position_diff(1)]) )...
            /norm(start_goal);
        if abs(l)<0.009 && abs(norm(position_diff)) < d && stop_wait>50
            state='move_straight';
            fprintf('changing FSM state to %s\n', state);
        end
    else
        error('Unknown state %s.\n', state);
    end
end

function R = Rotation_matrix(theta)
    R = [cos(theta), -sin(theta); 
        sin(theta), cos(theta)];
end

function R = rotationalMatrix(theta) 
    R=[cos(theta)   -sin(theta)   0; 
       sin(theta)   cos(theta)    0; 
       0            0             1];
end